# Use rust-based image for container
FROM rust:latest

# Set working directory in container
RUN mkdir /usr/src/miningpool
WORKDIR /usr/src/miningpool

# Copy all files
COPY src src
COPY Cargo.toml .
COPY LICENSE .

# Build release application
RUN cargo build --release

# Run the application
CMD ["target/release/skynet-token-mining-pool"]