# Unit tests that we run without the `testing` feature, to test certain
# functionality in production.
run-standard = \
	test_build_critical_smoke \
	test_build_var_smoke

all: build

audit:
	cargo audit -D warnings --ignore RUSTSEC-2021-0119

dependencies:
	rustup component add rustfmt
	rustup component add clippy
	cargo install cargo-audit

check:
	cargo check
	cargo check --features dev
	cargo check --features testing

fmt:
	cargo fmt

lint: fmt
	cargo clippy --all-targets --all-features -- -D warnings
	cargo fmt --all -- --check

build:
	cargo build

# Run all tests.
test-all: test-unit

# Run unit tests.
test-unit:
	cargo test --verbose --features testing -- --nocapture $(run)

# Run certain unit tests without the `testing` feature.
# Run in dev mode so we still get a backtrace.
test-unit-standard:
	cargo test --verbose --features dev -- --nocapture $(run-standard)

# Coverage command for GitLab CI. Linux-only.
coverage-ci:
	cargo tarpaulin --ignore-tests --out Xml --features testing

# Command for checking coverage locally.
#
# See https://github.com/xd009642/tarpaulin#docker.
coverage-docker:
	docker run --security-opt seccomp=unconfined -v "${PWD}:/volume" xd009642/tarpaulin sh -c "cargo tarpaulin --features testing"
