//! Collection of handlers for every route served by the API.

use crate::models::Statistics;

// Constants
const VERSION_V1: u8 = 1;

/// Returns the statistics
pub async fn stats() -> Result<impl warp::Reply, warp::Rejection> {
    let stats = &Statistics {
        version: VERSION_V1,
    };
    Ok(warp::reply::json(&stats))
}
