//! Skynet Token Mining Pool Library
#![deny(missing_docs)]

use std::net::{IpAddr, SocketAddr};

mod handlers;
mod models;
mod routes;

/// Skynet Token Mining Pool API
pub struct Api {
    /// The IP address at which the API is served
    pub ip: IpAddr,
    /// The port at which the API is served
    pub port: u16,
}

impl Api {
    /// serve starts the server
    pub async fn serve(&self) {
        let address = SocketAddr::new(self.ip, self.port);
        warp::serve(routes::all_routes()).run(address).await;
    }
}
