//! Skynet Token Mining Pool
extern crate core;

mod persist;

use skynet_token_mining_pool::Api;
use std::net::{IpAddr, Ipv4Addr};

// Server Constants
const IP: IpAddr = IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1));
const PORT: u16 = 3000;

#[tokio::main]
async fn main() {
    let api = Api { ip: IP, port: PORT };
    api.serve().await;
}
