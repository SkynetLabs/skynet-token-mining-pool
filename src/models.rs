//! Collection of models used by the API.

use serde::{Deserialize, Serialize};

/// Contains statistics about the mining pool
#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct Statistics {
    /// Version field
    pub version: u8,
}
