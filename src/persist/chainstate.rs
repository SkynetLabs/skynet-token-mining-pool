use anyhow::{anyhow, Result};
use std::fs::{File, OpenOptions};
use std::io;
use std::io::{BufWriter, Read, Seek, SeekFrom, Write};

// TODO: Remove this once we have the encoded leaf size in the token lib.
#[allow(dead_code)]
const LEAF_SIZE: usize = 128;

/// The size of the file header. This is some reserved space for metadata at the
/// beginning of the file.
#[allow(dead_code)]
const HEADER_SIZE: usize = 4096;

/// The current version of the chainstate file.
#[allow(dead_code)]
const VERSION: u8 = 1;

/// The type of the header.
struct Header {
    /// The version of the persistence.
    version: u8,
}

impl Header {
    /// Encodes the header into bytes.
    fn encode(&self) -> [u8; HEADER_SIZE] {
        let mut b = [0; HEADER_SIZE];
        b[0] = VERSION;
        b
    }

    /// Decodes a header from bytes.
    #[allow(dead_code)]
    fn decode(b: &[u8]) -> Result<Self> {
        if b.len() != HEADER_SIZE {
            return Err(anyhow!(format!(
                "invalid header size {} != {}",
                b.len(),
                HEADER_SIZE
            )));
        }
        let h = Header { version: b[0] };
        if h.version != VERSION {
            return Err(anyhow!(format!(
                "invalid version: {} != {}",
                h.version, VERSION
            )));
        }
        Ok(h)
    }
}

/// The persistence of all the leaves in the chain state.
pub struct ChainState {
    file: File,
    header: Header,
    num_leaves: u64,
}

impl ChainState {
    #[allow(dead_code)]
    fn new(file_path: &str) -> Result<Self> {
        let mut file = match OpenOptions::new().write(true).read(true).open(file_path) {
            Ok(file) => file,
            Err(err) => {
                if err.kind() == io::ErrorKind::NotFound {
                    return ChainState::init(file_path);
                }
                return Err(anyhow!(err));
            }
        };

        // Get the filesize.
        let size = file.metadata()?.len() as usize;
        if size < HEADER_SIZE {
            return Err(anyhow!(format!(
                "expected chain state file to have at least size {} but got {}",
                HEADER_SIZE, size
            )));
        }
        if (size - HEADER_SIZE) % LEAF_SIZE != 0 {
            return Err(anyhow!("size of chain state file is not leaf-aligned"));
        }
        let num_leaves = ((size - HEADER_SIZE) / LEAF_SIZE) as u64;

        // Existing file detected. Read the header.
        file.seek(SeekFrom::Start(0))?;
        let mut header_bytes = [0_u8; HEADER_SIZE];
        file.read_exact(&mut header_bytes)?;
        let header = Header::decode(&header_bytes)?;

        let chain_state = ChainState {
            header,
            file,
            num_leaves,
        };
        Ok(chain_state)
    }

    /// Creates a new ChainState on disk.
    #[allow(dead_code)]
    fn init(path: &str) -> Result<Self> {
        // Create file.
        let mut file = OpenOptions::new()
            .write(true)
            .read(true)
            .create_new(true)
            .open(path)?;

        // Write header and sync the file before returning it.
        let header = Header { version: VERSION };
        file.write_all(&header.encode())?;
        file.sync_all()?;
        Ok(ChainState {
            file,
            header,
            num_leaves: 0,
        })
    }

    /// Appends one or more new leaves.
    #[allow(dead_code)]
    fn append(&mut self, leaves: &[[u8; LEAF_SIZE]]) -> io::Result<()> {
        // Seek to the end of the file.
        let off = self.file.seek(SeekFrom::End(0_i64))?;
        assert_eq!(
            HEADER_SIZE + self.num_leaves as usize * LEAF_SIZE,
            off as usize
        );

        // Buffer the writes since it can potentially be a lot of leaves.
        let mut buffered_writer = BufWriter::new(&self.file);

        // Append the leaves.
        for leaf in leaves {
            buffered_writer.write_all(leaf)?;
            self.num_leaves += 1;
        }
        buffered_writer.flush()
    }

    /// Removes num_leaves leaves from the chainstate.
    #[allow(dead_code)]
    fn remove(&mut self, num_leaves: u64) -> Result<()> {
        // Update the header first.
        if self.num_leaves < num_leaves {
            return Err(anyhow!(
                "can't remove more leaves than present in the chainstate"
            ));
        }
        self.num_leaves -= num_leaves;

        // Truncate the file.
        self.file
            .set_len((HEADER_SIZE + self.num_leaves as usize * LEAF_SIZE) as u64)?;

        Ok(())
    }

    /// Reads the leaf at a given index.
    #[allow(dead_code)]
    fn read_leaf(&mut self, idx: u64) -> io::Result<[u8; LEAF_SIZE]> {
        self.file
            .seek(SeekFrom::Start(HEADER_SIZE as u64 + idx * LEAF_SIZE as u64))?;
        let mut leaf = [0_u8; LEAF_SIZE];
        self.file.read_exact(&mut leaf)?;
        Ok(leaf)
    }

    /// Writes the current header of the file to disk.
    #[allow(dead_code)]
    fn write_header(&mut self) -> io::Result<()> {
        self.file.seek(SeekFrom::Start(0))?;
        self.file.write_all(&self.header.encode())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand::RngCore;
    use std::io::{ErrorKind, Read};
    use std::path::PathBuf;
    use testdir::testdir;

    /// Helper that creates a new path for a chainstate.
    fn new_test_path() -> PathBuf {
        let dir = testdir!();
        dir.join("chainstate")
    }

    /// Helper that creates a random encoded leaf.
    fn random_leaf() -> [u8; LEAF_SIZE] {
        let mut leaf = [0_u8; LEAF_SIZE];
        rand::thread_rng().fill_bytes(&mut leaf);
        leaf
    }

    /// Unit test for init.
    #[test]
    fn init_file() {
        let file_path = new_test_path();
        let file_path_str = file_path.to_str().unwrap();

        // File shouldn't exist yet.
        assert!(!file_path.exists());
        assert!(!file_path.is_file());

        // Create chainstate file.
        let chain_state = ChainState::new(file_path_str).unwrap();

        // Check header.
        assert_eq!(chain_state.num_leaves, 0);
        assert_eq!(chain_state.header.version, VERSION);

        // File should be created now and have the size of the header.
        let md = file_path.metadata().unwrap();
        assert!(md.is_file());
        assert_eq!(md.len() as usize, HEADER_SIZE);

        // Check for valid header on disk.
        let mut file = File::open(&file_path).unwrap();
        let mut buf = [0_u8; HEADER_SIZE];
        file.read_exact(&mut buf).unwrap();
        let header = Header::decode(&buf).unwrap();
        assert_eq!(header.version, VERSION);
        assert_eq!(chain_state.num_leaves, 0);
    }

    /// Unit test for append.
    #[test]
    fn append() {
        // Create new chainstate.
        let file_path = new_test_path();
        let file_path_str = file_path.to_str().unwrap();
        let mut chain_state = ChainState::new(file_path_str).unwrap();

        // Append a leaf.
        let leaf = random_leaf();
        let leaves = [leaf];
        chain_state.append(&leaves).unwrap();

        // Check filesize and content.
        assert_eq!(
            chain_state.file.metadata().unwrap().len() as usize,
            HEADER_SIZE + LEAF_SIZE
        );
        let mut read_leaf = [0_u8; LEAF_SIZE];
        chain_state
            .file
            .seek(SeekFrom::Start(HEADER_SIZE as u64))
            .unwrap();
        chain_state.file.read_exact(&mut read_leaf).unwrap();
        assert_eq!(leaf, read_leaf);

        // Check header.
        assert_eq!(chain_state.header.version, VERSION);
        assert_eq!(chain_state.num_leaves, 1);

        // Append 2 more leaves.
        let leaf2 = random_leaf();
        let leaf3 = random_leaf();
        let leaves = [leaf2, leaf3];
        chain_state.append(&leaves).unwrap();

        // Check filesize and content.
        assert_eq!(
            chain_state.file.metadata().unwrap().len() as usize,
            HEADER_SIZE + 3 * LEAF_SIZE
        );
        let mut read_leaves = [0_u8; 2 * LEAF_SIZE];
        chain_state
            .file
            .seek(SeekFrom::Start((HEADER_SIZE + LEAF_SIZE) as u64))
            .unwrap();
        chain_state.file.read_exact(&mut read_leaves).unwrap();
        assert_eq!(read_leaves[..LEAF_SIZE], leaf2);
        assert_eq!(read_leaves[LEAF_SIZE..2 * LEAF_SIZE], leaf3);

        // Recreate chain state.
        let chain_state = ChainState::new(file_path_str).unwrap();

        // Check header and file size.
        assert_eq!(
            chain_state.file.metadata().unwrap().len() as usize,
            HEADER_SIZE + 3 * LEAF_SIZE
        );
        assert_eq!(chain_state.header.version, VERSION);
        assert_eq!(chain_state.num_leaves, 3);
    }

    /// Unit test for read_leaf.
    #[test]
    fn read_leaf() {
        // Create new chainstate.
        let file_path = new_test_path();
        let file_path_str = file_path.to_str().unwrap();
        let mut chain_state = ChainState::new(file_path_str).unwrap();

        // Append 2 leaves.
        let leaf1 = random_leaf();
        let leaf2 = random_leaf();
        let leaves = [leaf1, leaf2];
        chain_state.append(&leaves).unwrap();

        // Fetch the leaves with read_leaf.
        let read_leaf = chain_state.read_leaf(0).unwrap();
        assert_eq!(read_leaf, leaf1);

        let read_leaf = chain_state.read_leaf(1).unwrap();
        assert_eq!(read_leaf, leaf2);

        // Fetch beyond the boundary of the file.
        if let Err(err) = chain_state.read_leaf(2) {
            assert_eq!(err.kind(), ErrorKind::UnexpectedEof);
        } else {
            panic!("should have failed");
        }
    }

    /// Unit test for remove.
    #[test]
    fn remove() {
        // Create new chainstate.
        let file_path = new_test_path();
        let file_path_str = file_path.to_str().unwrap();
        let mut chain_state = ChainState::new(file_path_str).unwrap();

        // Append 3 leaves.
        let leaf1 = random_leaf();
        let leaf2 = random_leaf();
        let leaf3 = random_leaf();
        let leaves = [leaf1, leaf2, leaf3];
        chain_state.append(&leaves).unwrap();

        // Remove the leaves.
        chain_state.remove(1).unwrap();
        assert_eq!(chain_state.num_leaves, 2);
        chain_state.remove(2).unwrap();
        assert_eq!(chain_state.num_leaves, 0);

        // Try to read them. Should fail both times.
        if let Err(err) = chain_state.read_leaf(1) {
            assert_eq!(err.kind(), ErrorKind::UnexpectedEof);
        } else {
            panic!("should have failed");
        }
        if let Err(err) = chain_state.read_leaf(0) {
            assert_eq!(err.kind(), ErrorKind::UnexpectedEof);
        } else {
            panic!("should have failed");
        }

        // Append again. This time in different order.
        chain_state.append(&[leaf3, leaf2]).unwrap();
        assert_eq!(chain_state.num_leaves, 2);

        // Read the leaves and compare.
        assert_eq!(chain_state.read_leaf(0).unwrap(), leaf3);
        assert_eq!(chain_state.read_leaf(1).unwrap(), leaf2);

        // Remove more leaves than we have. Should fail.
        if let Err(err) = chain_state.remove(3) {
            assert_eq!(
                err.root_cause().to_string(),
                "can't remove more leaves than present in the chainstate"
            );
        } else {
            panic!("should have failed");
        }
    }
}
