#![allow(dead_code)]

#[cfg(test)]
mod tests;

use anyhow::anyhow;
use std::{
    cell::RefCell,
    fmt,
    ops::Not,
    rc::{Rc, Weak},
};

#[derive(Copy, Clone, Debug, PartialEq)]
enum Color {
    Red,
    Black,
}

impl Default for Color {
    fn default() -> Self {
        Color::Black
    }
}

/// Pointer to a RedBlackNode. Wrapped in an RC to allow for circular
/// referencing through strong and weak references.
#[derive(Debug)]
#[cfg_attr(any(feature = "testing", feature = "dev"), derive(PartialEq))]
struct NodePtr(Rc<RefCell<RedBlackNode>>);

/// Like NodePtr but with a weak reference to allow for child nodes to point to
/// their parent nodes.
type WeakNodePtr = Weak<RefCell<RedBlackNode>>;

/// Implement Clone for the NodePtr. We want to be able to clone them cheaply by
/// simply incrementing the reference counter.
impl Clone for NodePtr {
    fn clone(&self) -> Self {
        NodePtr(Rc::clone(&self.0))
    }
}

/// The RedBlackTree object. Points to the root of the tree unless empty.
#[derive(Debug)]
#[cfg_attr(any(feature = "testing", feature = "dev"), derive(PartialEq))]
struct RedBlackTree {
    root: Option<NodePtr>,
}

// TODO: remove this once we have a LeafID type in the token lib.
#[derive(Clone, Default, PartialEq)]
struct LeafID([u8; 32]);

// For better diff outputs from pretty_assertions.
impl fmt::Debug for LeafID {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // Write strictly the first element into the supplied output
        // stream: `f`. Returns `fmt::Result` which indicates whether the
        // operation succeeded or failed. Note that `write!` uses syntax which
        // is very similar to `println!`.
        write!(f, "{}", hex::encode(self.0))
    }
}

/// Node of a RedBlackTree. Usually wrapped within a NodePtr.
#[derive(Clone, Debug, Default)]
struct RedBlackNode {
    parent: Option<WeakNodePtr>, // None for root node
    left: Option<NodePtr>,       // left child
    right: Option<NodePtr>,      // right child

    color: Color,

    key: LeafID,

    #[cfg(any(feature = "testing", feature = "dev"))]
    id: String, // Useful for testing
}

// NOTE: This isn't very efficient. Implement for testing only for now.
#[cfg(any(feature = "testing", feature = "dev"))]
impl PartialEq for RedBlackNode {
    fn eq(&self, other: &Self) -> bool {
        // Check equality of the node data.
        fn node_data_eq(node1: &RedBlackNode, node2: &RedBlackNode) -> bool {
            node1.id == node2.id && node1.color == node2.color && node1.key == node2.key
        }
        // Check equality of all the parents without checking the parents'
        // children again.
        fn parent_eq(node1: &RedBlackNode, node2: &RedBlackNode) -> bool {
            if node1.parent.is_some() != node2.parent.is_some() {
                return false;
            }
            if let (Some(parent1), Some(parent2)) = (
                node1.parent.as_ref().and_then(|weak| weak.upgrade()),
                node2.parent.as_ref().and_then(|weak| weak.upgrade()),
            ) {
                let (parent1, parent2) = (parent1.borrow(), parent2.borrow());
                return node_data_eq(&parent1, &parent2) && parent_eq(&parent1, &parent2);
            }
            true
        }

        node_data_eq(self, other)
            && parent_eq(self, other)
            && self.left == other.left
            && self.right == other.right
    }
}
#[cfg(any(feature = "testing", feature = "dev"))]
impl Eq for RedBlackNode {}

impl NodePtr {
    /// Two NodePtrs are considered equal if they are both references to the
    /// same instance.
    fn ptr_eq(&self, other: &Self) -> bool {
        Rc::ptr_eq(&self.0, &other.0)
    }

    /// Creates a NodePtr from a RedBlackNode.
    fn from(node: RedBlackNode) -> Self {
        NodePtr(Rc::new(RefCell::new(node)))
    }

    /// Convenience method to return the parent if it exists.
    #[inline]
    fn parent(&self) -> Option<NodePtr> {
        (*self.0)
            .borrow()
            .parent
            .as_ref()
            .and_then(|weak_parent| weak_parent.upgrade())
            .map(NodePtr)
    }

    /// Convenience method to get a node's left child.
    #[inline]
    fn left(&self) -> Option<NodePtr> {
        (*self.0).borrow().left.clone()
    }

    /// Convenience method to get a node's right child.
    #[inline]
    fn right(&self) -> Option<NodePtr> {
        (*self.0).borrow().right.clone()
    }

    /// Convenience method to get a node's color.
    #[inline]
    fn color(&self) -> Color {
        (*self.0).borrow().color
    }

    /// Convenience method to get a node's child.
    fn child(&self, dir: Direction) -> Option<NodePtr> {
        match dir {
            Direction::Left => self.left(),
            Direction::Right => self.right(),
        }
    }

    /// Convenience method to set a node's child.
    fn set_child(&self, c: Option<NodePtr>, dir: Direction) {
        match dir {
            Direction::Left => self.set_left(c),
            Direction::Right => self.set_right(c),
        }
    }

    /// Convenience method to set a node's left child and the child's parent.
    #[inline]
    fn set_left(&self, c: Option<NodePtr>) {
        if let Some(child) = &c {
            (*child.0).borrow_mut().parent = Some(Rc::downgrade(&self.0));
        }
        (*self.0).borrow_mut().left = c;
    }

    /// Convenience method to set a node's right child and the child's parent.
    #[inline]
    fn set_right(&self, c: Option<NodePtr>) {
        if let Some(child) = &c {
            (*child.0).borrow_mut().parent = Some(Rc::downgrade(&self.0));
        }
        (*self.0).borrow_mut().right = c;
    }

    /// Convenience method to set a node's color.
    #[inline]
    fn set_color(&self, c: Color) {
        (*self.0).borrow_mut().color = c
    }

    /// Returns the direction of the node relative to its parent or None if the
    /// node doesn't have a parent.
    fn direction(&self) -> Option<Direction> {
        if let Some(parent) = self.parent() {
            if let Some(parent_left) = parent.left() {
                if parent_left.ptr_eq(self) {
                    return Some(Direction::Left);
                }
            }
            if let Some(parent_right) = parent.right() {
                if parent_right.ptr_eq(self) {
                    return Some(Direction::Right);
                }
            }
            // This should never be reached. Panic?
        }
        None
    }
}

#[derive(Clone, Copy)]
enum Direction {
    Left,
    Right,
}

impl Not for Direction {
    type Output = Self;
    fn not(self) -> Self::Output {
        match self {
            Direction::Left => Direction::Right,
            Direction::Right => Direction::Left,
        }
    }
}

impl RedBlackTree {
    // Rotates a (sub-) tree in the given direction.
    fn rotate(&mut self, root: &NodePtr, dir: Direction) -> anyhow::Result<NodePtr> {
        match dir {
            Direction::Left => self.rotate_left(root),
            Direction::Right => self.rotate_right(root),
        }
    }
    // Rotates a (sub-) tree to the left.
    fn rotate_left(&mut self, root: &NodePtr) -> anyhow::Result<NodePtr> {
        let root_parent = root.parent();
        let root_right = root.right();

        // Make sure the root has a right child.
        if root_right.is_none() {
            return Err(anyhow!("rotate_left requires root to have a right child"));
        }
        let root_right = root_right.unwrap();
        let root_right_left = root_right.left();

        root.set_right(root_right_left);
        root_right.set_left(Some(root.clone()));

        if let Some(root_parent) = root_parent {
            if root_parent
                .right()
                .map_or(false, |right_child| root.ptr_eq(&right_child))
            {
                root_parent.set_right(Some(root_right.clone()));
            } else {
                root_parent.set_left(Some(root_right.clone()));
            }
        } else {
            (*root_right.0).borrow_mut().parent = None;
            self.root = Some(root_right.clone());
        }

        // Return new root of subtree.
        Ok(root_right)
    }

    // Rotates a (sub-) tree right.
    fn rotate_right(&mut self, root: &NodePtr) -> anyhow::Result<NodePtr> {
        let root_parent = root.parent();
        let root_left = root.left();

        // Make sure the root has a left child.
        if root_left.is_none() {
            return Err(anyhow!("rotate_right requires root to have a left child"));
        }
        let root_left = root_left.unwrap();
        let root_left_right = root_left.right();

        root.set_left(root_left_right);
        root_left.set_right(Some(root.clone()));

        if let Some(root_parent) = root_parent {
            if root_parent
                .right()
                .map_or(false, |right_child| root.ptr_eq(&right_child))
            {
                root_parent.set_right(Some(root_left.clone()));
            } else {
                root_parent.set_left(Some(root_left.clone()));
            }
        } else {
            (*root_left.0).borrow_mut().parent = None;
            self.root = Some(root_left.clone());
        }

        // Return new root of subtree.
        Ok(root_left)
    }

    /// Inserts a node into the tree under the given parent. If parent is None,
    /// the node will become the new root of the tree. The direction determines
    /// whether the node is inserted as a left or right child. After insertion,
    /// the tree will re-balance itself.
    ///
    /// # Panics
    ///
    /// If parent is not None and already has a child at the given dir,
    /// insert panics. It should only be called after a search operation that
    /// verifies the node is placed in a correct location.
    fn insert(&mut self, node: &NodePtr, parent: Option<&NodePtr>, dir: Direction) {
        // Set the new node's color to red.
        node.set_color(Color::Red);

        // Check if a parent was passed in.
        if let Some(parent) = parent {
            // If a parent was passed in, set it as the node's parent and also
            // set the node as the parent's child.
            assert!(parent.child(dir).is_none());
            parent.set_child(Some(node.clone()), dir);
        } else {
            // Can't insert a node as root if another root already exists. It
            // should be removed first.
            assert!(self.root.is_none());
            // If not, the node is inserted as the new root of the tree.
            self.root = Some(node.clone());
            return; // done
        }

        // Run re-balancing loop.
        self.rebalance(node);
    }

    /// Re-balances the red-black tree after modifying it.
    fn rebalance(&mut self, node: &NodePtr) {
        // Run re-balancing loop.
        let mut current_node = node.clone();
        while current_node.parent().is_some() {
            let mut parent = current_node.parent().clone().unwrap();

            // Case 1 - parent is black.
            if parent.color() == Color::Black {
                return; // insertion complete
            }

            // Case 4 - parent is root and red.
            if parent.parent().is_none() {
                parent.set_color(Color::Black);
                return; // insertion complete
            }

            // Fetch the grandparent. It's guaranteed to be Some.
            let grandparent = parent.parent().unwrap();

            // Get the side of the parent in relation to the grandparent and
            // using that its uncle. We know the grandparent exists so
            // parent_side can't be None.
            let parent_side = parent.direction().unwrap();
            let uncle = grandparent.child(!parent_side);

            // Case 56 - parent is red and uncle is None or black.
            if uncle
                .as_ref()
                .map_or(true, |uncle| uncle.color() == Color::Black)
            {
                // Case 5 - parent red and uncle black and current_node is
                // inner grandchild of grandparent.
                if parent
                    .child(!parent_side)
                    .map_or(false, |child| child.ptr_eq(&current_node))
                {
                    let result = self.rotate(&parent, parent_side);
                    result.unwrap(); // should always succeed.

                    // current_node = parent; value never used, line kept for compliance with wikipedia spec.
                    parent = grandparent.child(parent_side).unwrap(); // should always work.
                }

                // Case 6 - parent red and uncle black and current_node outer
                // grandchild of grandparent.
                let result = self.rotate(&grandparent, !parent_side);
                result.unwrap(); // should always succeed.
                parent.set_color(Color::Black);
                grandparent.set_color(Color::Red);
                return; // insertion complete
            }
            let uncle = uncle.unwrap();

            // Case 2 - parent and uncle red.
            parent.set_color(Color::Black);
            uncle.set_color(Color::Black);
            grandparent.set_color(Color::Red);
            current_node = grandparent.clone();
        }
    }
}
