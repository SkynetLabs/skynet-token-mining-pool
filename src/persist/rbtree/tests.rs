use super::*;
use pretty_assertions::assert_eq;

/// Helper type for configuring rotation tests.
enum RootParentPos {
    None,
    Left,
    Right,
}

fn new_node(id: &str) -> RedBlackNode {
    RedBlackNode {
        id: id.into(),
        ..Default::default()
    }
}

fn new_node_ptr(id: &str) -> NodePtr {
    let node = new_node(id);
    NodePtr::from(node)
}

fn new_node_ptr_with(
    id: &str,
    color: Color,
    left: Option<NodePtr>,
    right: Option<NodePtr>,
) -> NodePtr {
    let node = new_node_ptr(id);
    node.set_color(color);
    node.set_left(left);
    node.set_right(right);
    node
}

/*
A macro for building trees in a convenient, somewhat visualizable way.

# Example

```rust
let expected_tree = make_tree!{
    ("root", Color::Black) => {
        ("root_left", Color::Red) => {
            ("root_left_left", Color::Black),
            ("root_left_right", Color::Black) => {
                None,
                ("root_left_right_right", Color::Red),
            },
        },
        ("root_right", Color::Black),
    },
};
```
*/

macro_rules! make_tree {
        (($id:expr, $color:expr)) => {
            RedBlackTree { root: Some(new_node_ptr_with($id, $color, None, None)) }
        };
        (($id:expr, $color:expr) => { $($t:tt)* }) => {
            {
                let (left, right) = make_tree_inner!($($t)*);
                RedBlackTree { root: Some(new_node_ptr_with($id, $color, left, right)) }
            }
        }
    }

macro_rules! make_tree_inner {
        (($id:expr, $color:expr), None) => {
            (Some(new_node_ptr_with($id, $color, None, None)), None)
        };
        (None, ($id:expr, $color:expr)) => {
            (None, Some(new_node_ptr_with($id, $color, None, None)))
        };
        (($id1:expr, $color1:expr), ($id2:expr, $color2:expr)) => {
            (
                Some(new_node_ptr_with($id1, $color1, None, None)),
                Some(new_node_ptr_with($id2, $color2, None, None)),
            )
        };
        (($id1:expr, $color1:expr) => { $($more1:tt)* }, None) => {
            {
                let (left_left, left_right) = make_tree_inner!($($more1)*);
                (Some(new_node_ptr_with($id1, $color1, left_left, left_right)), None)
            }
        };
        (None, ($id2:expr, $color2:expr) => { $($more2:tt)* }) => {
            {
                let (right_left, right_right) = make_tree_inner!($($more2)*);
                (None, Some(new_node_ptr_with($id2, $color2, right_left, right_right)))
            }
        };
        (($id1:expr, $color1:expr) => { $($more1:tt)* }, ($id2:expr, $color2:expr)) => {
            {
                let (left_left, left_right) = make_tree_inner!($($more1)*);
                let left = new_node_ptr_with($id1, $color1, left_left, left_right);
                (Some(left), Some(new_node_ptr_with($id2, $color2, None, None)))
            }
        };
        (($id1:expr, $color1:expr), ($id2:expr, $color2:expr) => { $($more2:tt)* }) => {
            {
                let (right_left, right_right) = make_tree_inner!($($more2)*);
                let right = new_node_ptr_with($id2, $color2, right_left, right_right);
                (Some(new_node_ptr_with($id1, $color1, None, None)), Some(right))
            }
        };
        (
            ($id1:expr, $color1:expr) => { $($more1:tt)* },
            ($id2:expr, $color2:expr) => { $($more2:tt)* }) =>
        {
            {
                let (left_left, left_right) = make_tree_inner!($($more1)*);
                let left = new_node_ptr_with($id1, $color1, left_left, left_right);
                let (right_left, right_right) = make_tree_inner!($($more2)*);
                let right = new_node_ptr_with($id2, $color2, right_left, right_right);
                (Some(left), Some(right))
            }
        };
    }

#[test]
fn insert() {
    // Create empty tree
    let mut tree = RedBlackTree { root: None };

    // Insert a node as root. It should be red.
    //
    // R
    //
    let root = new_node_ptr("root");
    tree.insert(&root, None, Direction::Left);
    let expected_tree = make_tree! {
        ("root", Color::Red)
    };
    assert_eq!(tree, expected_tree);

    // Insert another one as the left child of root. This should be red now
    // and flip root to be black.
    //
    //   B
    //  /
    // R
    let root_left = new_node_ptr("root_left");
    tree.insert(&root_left, Some(&root), Direction::Left);
    let expected_tree = make_tree! {
        ("root", Color::Black) => {
            ("root_left", Color::Red),
            None
        }
    };
    assert_eq!(tree, expected_tree);

    // Insert another one as the right child of root. That one should also
    // be red.
    //
    //   B
    //  / \
    // R   R
    let root_right = new_node_ptr("root_right");
    tree.insert(&root_right, Some(&root), Direction::Right);
    let expected_tree = make_tree! {
        ("root", Color::Black) => {
            ("root_left", Color::Red),
            ("root_right", Color::Red)
        }
    };
    assert_eq!(tree, expected_tree);

    // Insert another node as the left child of root_left. That one should
    // be red, flip root_left and root_right to be black. The root is
    // red again.
    //
    //     R
    //    / \
    //   B   B
    //  /
    // R
    let root_left_left = new_node_ptr("root_left_left");
    tree.insert(&root_left_left, Some(&root_left), Direction::Left);
    let expected_tree = make_tree! {
        ("root", Color::Red) => {
            ("root_left", Color::Black) => {
                ("root_left_left", Color::Red),
                None
            },
            ("root_right", Color::Black)
        }
    };
    assert_eq!(tree, expected_tree);

    // Insert another node as the left child of root_left_left. That should
    // be inserted as red, but is then rotated and rebalanced.
    //
    //       R
    //      / \
    //     B   B
    //    /
    //   R
    //  /
    // R
    //
    // rotates into:
    //
    //       R
    //      / \
    //     B   B
    //    / \
    //   R   R
    let root_left_left_left = new_node_ptr("root_left_left_left");
    tree.insert(&root_left_left_left, Some(&root_left_left), Direction::Left);
    let expected_tree = make_tree! {
        ("root", Color::Red) => {
            ("root_left_left", Color::Black) => {
                ("root_left_left_left", Color::Red),
                ("root_left", Color::Red)
            },
            ("root_right", Color::Black)
        }
    };
    assert_eq!(tree, expected_tree);

    // Reassign the rotated variables to make them match the graph again.
    let root_left_right = root_left.clone();
    let _root_left = root_left_left;
    let _root_left_left = root_left_left_left;

    // Insert another node root_left_right_left. This should flip the left
    // side of the tree but not root_right.
    //
    //       B
    //      / \
    //     R   B
    //    / \
    //   B   B
    //      /
    //     R
    let root_left_right_left = new_node_ptr("root_left_right_left");
    tree.insert(
        &root_left_right_left,
        Some(&root_left_right),
        Direction::Left,
    );
    let expected_tree = make_tree! {
        ("root", Color::Black) => {
            ("root_left_left", Color::Red) => {
                ("root_left_left_left", Color::Black),
                ("root_left", Color::Black) => {
                    ("root_left_right_left", Color::Red),
                    None
                }
            },
            ("root_right", Color::Black)
        }
    };
    assert_eq!(tree, expected_tree);

    // Insert another node root_left_right_left. This should flip the left
    // side of the tree but not root_right.
    //
    //       B
    //      / \
    //     R   B
    //    / \
    //   B   B
    //      /
    //     R
    //      \
    //       R
    //
    // rotates into:
    //
    //       B
    //      / \
    //     R   B
    //    / \
    //   B   B
    //      / \
    //     R   R
    let root_left_right_left_left = new_node_ptr("root_left_right_left");
    tree.insert(
        &root_left_right_left_left,
        Some(&root_left_right_left),
        Direction::Right,
    );

    let expected_tree = make_tree! {
        ("root", Color::Black) => {
            ("root_left_left", Color::Red) => {
                ("root_left_left_left", Color::Black),
                ("root_left_right_left", Color::Black) => {
                    ("root_left_right_left", Color::Red),
                    ("root_left", Color::Red)
                }
            },
            ("root_right", Color::Black)
        }
    };
    assert_eq!(tree, expected_tree);
}

#[test]
fn test_make_tree() {
    let tree = make_tree! {
        ("root", Color::Red)
    };
    let root = tree.root.unwrap();
    let expected_root = new_node_ptr_with("root", Color::Red, None, None);
    assert_eq!(root, expected_root);
    assert!(root.left().is_none());
    assert!(root.right().is_none());
    assert_eq!(root.color(), Color::Red);

    let tree = make_tree! {
        ("root", Color::Black) => {
            ("root_left", Color::Red),
            None
        }
    };
    let root = tree.root.unwrap();
    let root_left = root.left().unwrap();
    let expected_root_left = new_node_ptr_with("root_left", Color::Red, None, None);
    let expected_root =
        new_node_ptr_with("root", Color::Black, Some(expected_root_left.clone()), None);
    assert_eq!(root, expected_root);
    assert_eq!(root_left, expected_root_left);
    assert!(root_left.left().is_none());
    assert!(root_left.right().is_none());
    assert!(root.right().is_none());
    assert_eq!(root_left.color(), Color::Red);
    assert_eq!(root.color(), Color::Black);

    let tree = make_tree! {
        ("root", Color::Black) => {
            ("root_left", Color::Red) => {
                ("root_left_left", Color::Black),
                ("root_left_right", Color::Black) => {
                    ("root_left_right_left", Color::Red),
                    ("root_left_right_right", Color::Red)
                }
            },
            ("root_right", Color::Black)
        }
    };
    let root = tree.root.unwrap();
    let root_left = root.left().unwrap();
    let root_left_left = root_left.left().unwrap();
    let root_left_right = root_left.right().unwrap();
    let root_left_right_left = root_left_right.left().unwrap();
    let root_left_right_right = root_left_right.right().unwrap();
    let root_right = root.right().unwrap();
    let expected_root_left_right_left =
        new_node_ptr_with("root_left_right_left", Color::Red, None, None);
    let expected_root_left_right_right =
        new_node_ptr_with("root_left_right_right", Color::Red, None, None);
    let expected_root_left_left = new_node_ptr_with("root_left_left", Color::Black, None, None);
    let expected_root_left_right = new_node_ptr_with(
        "root_left_right",
        Color::Black,
        Some(expected_root_left_right_left.clone()),
        Some(expected_root_left_right_right.clone()),
    );
    let expected_root_left = new_node_ptr_with(
        "root_left",
        Color::Red,
        Some(expected_root_left_left.clone()),
        Some(expected_root_left_right.clone()),
    );
    let expected_root_right = new_node_ptr_with("root_right", Color::Black, None, None);
    let expected_root = new_node_ptr_with(
        "root",
        Color::Black,
        Some(expected_root_left.clone()),
        Some(expected_root_right.clone()),
    );

    assert_eq!(root, expected_root);
    assert_eq!(root_left, expected_root_left);
    assert_eq!(root_right, expected_root_right);
    assert_eq!(root_left_left, expected_root_left_left);
    assert_eq!(root_left_right, expected_root_left_right);
    assert_eq!(root_left_right_left, expected_root_left_right_left);
    assert_eq!(root_left_right_right, expected_root_left_right_right);

    assert!(root_left_right_left.left().is_none());
    assert!(root_left_right_left.right().is_none());
    assert!(root_left_right_right.left().is_none());
    assert!(root_left_right_right.right().is_none());
    assert!(root_left_left.left().is_none());
    assert!(root_left_left.right().is_none());
    assert!(root_right.left().is_none());
    assert!(root_right.right().is_none());

    assert_eq!(root_left_right_right.color(), Color::Red);
    assert_eq!(root_left_right_left.color(), Color::Red);
    assert_eq!(root_left_left.color(), Color::Black);
    assert_eq!(root_left_right.color(), Color::Black);
    assert_eq!(root_left.color(), Color::Red);
    assert_eq!(root_right.color(), Color::Black);
    assert_eq!(root.color(), Color::Black);
}

/// Unit test for rotate_right.
#[test]
fn rotate_right_without_root_parent() {
    rotate_right_with_pos(RootParentPos::None);
}

/// Unit test for rotate_right.
#[test]
fn rotate_right_with_root_as_left_child() {
    rotate_right_with_pos(RootParentPos::Left);
}

/// Unit test for rotate_right.
#[test]
fn rotate_right_with_root_as_right_child() {
    rotate_right_with_pos(RootParentPos::Right);
}

/// Unit test for rotate_left.
#[test]
fn rotate_left_without_root_parent() {
    rotate_left_with_pos(RootParentPos::None);
}

/// Unit test for rotate_left.
#[test]
fn rotate_left_with_root_as_left_child() {
    rotate_left_with_pos(RootParentPos::Left);
}

/// Unit test for rotate_left.
#[test]
fn rotate_left_with_root_as_right_child() {
    rotate_left_with_pos(RootParentPos::Right);
}

/// Unit test for rotate_right. The input determines the position of the
/// root node relative to its parent.
fn rotate_right_with_pos(root_parent_pos: RootParentPos) {
    // Prepare the following tree structure for testing.
    // NOTE: The root_parent is either None or has root as its left or right
    // child depending on the root_parent_pos.
    //
    //                         root_parent
    //                             |
    //                            root
    //                      /            \
    //                 /                      \
    //            root_left                root_right
    //         /           \
    // root_left_left root_left_right
    //
    let root_tree = make_tree! {
        ("root", Color::Black) => {
            ("root_left", Color::Black) => {
                ("root_left_left", Color::Black),
                ("root_left_right", Color::Black)
            },
            ("root_right", Color::Black)
        }
    };
    let root = root_tree.root.unwrap();

    let root_sibling = new_node_ptr("root_sibling");
    let root_parent = new_node_ptr("root_parent");
    match root_parent_pos {
        RootParentPos::None => {}
        RootParentPos::Left => {
            root_parent.set_left(Some(root.clone()));
            root_parent.set_right(Some(root_sibling.clone()));
        }
        RootParentPos::Right => {
            root_parent.set_left(Some(root_sibling.clone()));
            root_parent.set_right(Some(root.clone()));
        }
    }

    // Create tree with the root parent.
    let mut tree = match root_parent_pos {
        RootParentPos::None => RedBlackTree {
            root: Some(root.clone()),
        },
        _ => RedBlackTree {
            root: Some(root_parent.clone()),
        },
    };

    // Rotate the (sub)-tree to the right. The result should look like this.
    //
    //                         root_parent
    //                             |
    //                          root_left
    //                      /              \
    //                 /                       \
    //         root_left_left                  root
    //                                     /          \
    //                             root_left_right root_right
    //
    let new_root = tree.rotate_right(&root).unwrap();
    let new_root_left = new_root.left().unwrap();
    let new_root_right = new_root.right().unwrap();
    let new_root_right_left = new_root_right.left().unwrap();
    let new_root_right_right = new_root_right.right().unwrap();

    // Check that the previous nodes are in the right spots.
    let expected_tree = make_tree! {
        ("root_left", Color::Black) => {
            ("root_left_left", Color::Black),
            ("root", Color::Black) => {
                ("root_left_right", Color::Black),
                ("root_right", Color::Black)
            }
        }
    };
    // Set the root parent.
    let expected_tree_root = expected_tree.root;
    let expected_tree_root_parent = new_node_ptr("root_parent");
    match root_parent_pos {
        RootParentPos::None => {}
        RootParentPos::Left => expected_tree_root_parent.set_left(expected_tree_root.clone()),
        RootParentPos::Right => expected_tree_root_parent.set_right(expected_tree_root.clone()),
    }
    assert_eq!(new_root, expected_tree_root.unwrap());

    // Check children.
    match root_parent_pos {
        RootParentPos::None => {}
        RootParentPos::Left => {
            assert!(root_parent.left().unwrap().ptr_eq(&new_root));
            assert!(root_parent.right().unwrap().ptr_eq(&root_sibling));
        }
        RootParentPos::Right => {
            assert!(root_parent.left().unwrap().ptr_eq(&root_sibling));
            assert!(root_parent.right().unwrap().ptr_eq(&new_root))
        }
    }

    // Check parents.
    assert!(root_parent.parent().is_none());
    match root_parent_pos {
        RootParentPos::None => assert!(new_root.parent().is_none()),
        RootParentPos::Left => {
            assert!(new_root.parent().unwrap().ptr_eq(&root_parent));
            assert!(root_sibling.parent().unwrap().ptr_eq(&root_parent));
        }
        RootParentPos::Right => {
            assert!(new_root.parent().unwrap().ptr_eq(&root_parent));
            assert!(root_sibling.parent().unwrap().ptr_eq(&root_parent));
        }
    }

    // Check reference count.
    match root_parent_pos {
        RootParentPos::None => {
            assert_eq!(Rc::weak_count(&root_parent.0), 0);
            assert_eq!(Rc::strong_count(&root_parent.0), 1);

            assert_eq!(Rc::weak_count(&new_root.0), 2);
            assert_eq!(Rc::strong_count(&new_root.0), 2);
        }
        _ => {
            assert_eq!(Rc::weak_count(&root_parent.0), 2);
            assert_eq!(Rc::strong_count(&root_parent.0), 2);

            assert_eq!(Rc::weak_count(&new_root.0), 2);
            assert_eq!(Rc::strong_count(&new_root.0), 2);
        }
    }

    assert_eq!(Rc::weak_count(&new_root_left.0), 0);
    assert_eq!(Rc::strong_count(&new_root_left.0), 2);

    assert_eq!(Rc::weak_count(&new_root_right.0), 2);
    assert_eq!(Rc::strong_count(&new_root_right.0), 3);

    assert_eq!(Rc::weak_count(&new_root_right_left.0), 0);
    assert_eq!(Rc::strong_count(&new_root_right_left.0), 2);

    assert_eq!(Rc::weak_count(&new_root_right_right.0), 0);
    assert_eq!(Rc::strong_count(&new_root_right_right.0), 2);
}

/// Unit test for rotate_left. The input determines the position of the root
/// node relative to its parent.
fn rotate_left_with_pos(root_parent_pos: RootParentPos) {
    // Prepare the following tree structure for testing.
    // NOTE: The root_parent is either None or has root as its left or right
    // child depending on the root_parent_pos.
    //
    //                         root_parent
    //                             |
    //                            root
    //                      /            \
    //                 /                      \
    //            root_left                root_right
    //                                  /           \
    //                          root_right_left root_right_right
    //
    let root_tree = make_tree! {
        ("root", Color::Black) => {
            ("root_left", Color::Black),
            ("root_right", Color::Black) => {
                ("root_right_left", Color::Black),
                ("root_right_right", Color::Black)
            }
        }
    };
    let root = root_tree.root.unwrap();

    let root_sibling = new_node_ptr("root_sibling");
    let root_parent = new_node_ptr("root_parent");
    match root_parent_pos {
        RootParentPos::None => {}
        RootParentPos::Left => {
            root_parent.set_left(Some(root.clone()));
            root_parent.set_right(Some(root_sibling.clone()));
        }
        RootParentPos::Right => {
            root_parent.set_left(Some(root_sibling.clone()));
            root_parent.set_right(Some(root.clone()));
        }
    }

    // Create tree with the root parent.
    let mut tree = match root_parent_pos {
        RootParentPos::None => RedBlackTree {
            root: Some(root.clone()),
        },
        _ => RedBlackTree {
            root: Some(root_parent.clone()),
        },
    };

    // Rotate the (sub)-tree to the left. The result should look like this.
    //
    //                         root_parent
    //                             |
    //                          root_right
    //                      /              \
    //                 /                       \
    //              root                   root_right_right
    //          /          \
    //   root_left   root_right_left
    //
    let new_root = tree.rotate_left(&root).unwrap();
    let new_root_left = new_root.left().unwrap();
    let new_root_right = new_root.right().unwrap();
    let new_root_left_left = new_root_left.left().unwrap();
    let new_root_left_right = new_root_left.right().unwrap();

    // Check that the previous nodes are in the right spots.
    let expected_tree = make_tree! {
        ("root_right", Color::Black) => {
            ("root", Color::Black) => {
                ("root_left", Color::Black),
                ("root_right_left", Color::Black)
            },
            ("root_right_right", Color::Black)
        }
    };
    // Set the root parent.
    let expected_tree_root = expected_tree.root;
    let expected_tree_root_parent = new_node_ptr("root_parent");
    match root_parent_pos {
        RootParentPos::None => {}
        RootParentPos::Left => expected_tree_root_parent.set_left(expected_tree_root.clone()),
        RootParentPos::Right => expected_tree_root_parent.set_right(expected_tree_root.clone()),
    }
    assert_eq!(new_root, expected_tree_root.unwrap());

    // Check children.
    match root_parent_pos {
        RootParentPos::None => {}
        RootParentPos::Left => {
            assert!(root_parent.left().unwrap().ptr_eq(&new_root));
            assert!(root_parent.right().unwrap().ptr_eq(&root_sibling));
        }
        RootParentPos::Right => {
            assert!(root_parent.left().unwrap().ptr_eq(&root_sibling));
            assert!(root_parent.right().unwrap().ptr_eq(&new_root))
        }
    }

    // Check parents.
    assert!(root_parent.parent().is_none());
    match root_parent_pos {
        RootParentPos::None => assert!(new_root.parent().is_none()),
        RootParentPos::Left => {
            assert!(new_root.parent().unwrap().ptr_eq(&root_parent));
            assert!(root_sibling.parent().unwrap().ptr_eq(&root_parent));
        }
        RootParentPos::Right => {
            assert!(new_root.parent().unwrap().ptr_eq(&root_parent));
            assert!(root_sibling.parent().unwrap().ptr_eq(&root_parent));
        }
    }

    // Check reference count.
    match root_parent_pos {
        RootParentPos::None => {
            assert_eq!(Rc::weak_count(&root_parent.0), 0);
            assert_eq!(Rc::strong_count(&root_parent.0), 1);

            assert_eq!(Rc::weak_count(&new_root.0), 2);
            assert_eq!(Rc::strong_count(&new_root.0), 2);
        }
        _ => {
            assert_eq!(Rc::weak_count(&root_parent.0), 2);
            assert_eq!(Rc::strong_count(&root_parent.0), 2);

            assert_eq!(Rc::weak_count(&new_root.0), 2);
            assert_eq!(Rc::strong_count(&new_root.0), 2);
        }
    }

    assert_eq!(Rc::weak_count(&new_root_left.0), 2);
    assert_eq!(Rc::strong_count(&new_root_left.0), 3);

    assert_eq!(Rc::weak_count(&new_root_right.0), 0);
    assert_eq!(Rc::strong_count(&new_root_right.0), 2);

    assert_eq!(Rc::weak_count(&new_root_left_left.0), 0);
    assert_eq!(Rc::strong_count(&new_root_left_left.0), 2);

    assert_eq!(Rc::weak_count(&new_root_left_right.0), 0);
    assert_eq!(Rc::strong_count(&new_root_left_right.0), 2);
}

/// Unit test for rotate_right.
#[test]
#[should_panic(expected = "rotate_right requires root to have a left child")]
fn rotate_right_without_left_child() {
    let mut tree = make_tree! {
        ("root", Color::Black) => {
            None,
            ("root_right", Color::Black)
        }
    };
    let root = tree.root.clone().unwrap();

    // Rotate should fail.
    let _new_root = tree.rotate_right(&root).unwrap();
}

/// Unit test for rotate_left.
#[test]
#[should_panic(expected = "rotate_left requires root to have a right child")]
fn rotate_left_without_right_child() {
    let mut tree = make_tree! {
        ("root", Color::Black) => {
            ("root_left", Color::Black),
            None
        }
    };
    let root = tree.root.clone().unwrap();

    // Rotate should fail.
    let _new_root = tree.rotate_left(&root).unwrap();
}
