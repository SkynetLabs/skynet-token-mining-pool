//! Collection of routes served by the Skynet Token Mining Pool API

use std::str;
use warp::{self, Filter};

use crate::handlers;

#[cfg(test)]
use crate::models::Statistics;

/// Returns all routes served by the Skynet Token Mining Pool API.
pub fn all_routes() -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    stats().with(warp::cors().allow_any_origin())
}

/// Returns mining pool statistics
pub fn stats() -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    warp::path!("stats")
        .and(warp::get())
        .and_then(handlers::stats)
}

#[cfg(test)]
#[tokio::test]
async fn test_stats() {
    let filter = stats();

    let res = warp::test::request().path("/stats").reply(&filter).await;
    assert_eq!(res.status(), 200, "expected 200 OK");

    let body_str = str::from_utf8(res.body()).unwrap();
    let stats: Statistics = serde_json::from_str(body_str).unwrap();
    assert_eq!(stats.version, 1, "expected version 1");
}
